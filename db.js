const mongoose = require('mongoose');
const uri = process.env.ATLAS_CON_STRING;
require('dotenv').config();


mongoose.connect(uri)
.then(() => {
    console.log('db connected...');
})
.catch(err => console.log(err));
