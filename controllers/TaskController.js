const express = require("express");
const router = express.Router();
const Task = require('./../models/Task');

router.use(express.json());


// CREATES A NEW TASK
router.post("/", function(req, res) {
    Task.create(
    {
      title: req.body.title,
      completed: req.body.completed
    },
    function(err, task) {
      if (err)
        return res
          .status(500)
          .send("There was a problem adding the information to the database.");
      res.status(200).send(task);
      }
    );
  });
  
  // RETURNS ALL THE TASKS IN THE DATABASE
  router.get("/", function(req, res) {
    Task.find({}, function(err, tasks) {
      if (err)
        return res.status(500).send("There was a problem finding the tasks.");
      res.status(200).send(tasks);
      });
  });
  
  // GETS A SINGLE TASK FROM THE DATABASE
    router.get('/:id', function (req, res) {
        const id = req.params.id;
        //console.log(id);
        Task.findById(id, function (err, task) {
        if (err)
            return res.status(500).send('There was a problem finding the task.');
        if (!task) return res.status(404).send('No task found with id ' + req.params.id);
        res.status(200).send(task);
        console.log(task);
        });
  }); 

// DELETE
    router.delete('/:id', function (req, res) {
        Task.findByIdAndRemove(req.params.id, function (err, task) {
        if (err)
            return res.status(500).send('There was a problem deleting the task with id: ' + req.params.id);
        res.status(200).send('task id ' + task.id + ' with the title '+ task.title + ' was deleted.');
        });
  });
  
  // UPDATE
    router.put('/:id', function (req, res) {
        Task.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true },
        function (err, task) {
            if (err)
            return res
                .status(500)
                .send('There was a problem updating the task. ' + err.message);
            res.status(200).send(task);
        }
        );
  });
  
  module.exports = router;
  
  